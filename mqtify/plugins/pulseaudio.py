import time
from pulsectl import Pulse

from yapsy.IPlugin import IPlugin
from yapsy.PluginManager import PluginManagerSingleton


class pulseaudio(IPlugin):
    '''PulseAudio plugin.

    Control PulseAudio from remote.
    '''
    CONF_SECTION = 'PulseAudio'
    TOPIC_PUB_VOL = '/mqtify/pulseaudio/vol'
    TOPIC_PUB_SOUND = '/mqtify/pulseaudio/sound'
    TOPIC_SUB = '/mqtify/pulseaudio/set'
    SINK_ID = 0
    TIMEOUT = 15

    def __init__(self):
        super().__init__()
        self._setup_mqtify_attr()
        self._setup_conf()
        self._setup_pulse_ctl()

    def _setup_mqtify_attr(self):
        '''Get attributes from Mqtify using manager singleton.'''
        manager = PluginManagerSingleton.get()
        self.mqtt = manager.mqtify.mqtt
        self.conf = manager.mqtify.conf

    def _setup_conf(self):
        '''Read configuration.'''
        if self.CONF_SECTION in self.conf:
            conf = self.conf[self.CONF_SECTION]
            if 'topic_pub_vol' in conf:
                self.TOPIC_PUB_VOL = str(conf['topic_pub_vol'])
            if 'topic_pub_sound' in conf:
                self.TOPIC_PUB_SOUND = str(conf['topic_pub_sound'])
            if 'topic_sub' in conf:
                self.TOPIC_SUB = str(conf['topic_sub'])
            if 'timeout' in conf:
                self.TIMEOUT = int(conf['timeout'])
            if 'sink_id' in conf:
                self.SINK_ID = int(conf['sink_id'])

    def _setup_pulse_ctl(self):
        '''Get pulse mixer.'''
        self.pulsectl = Pulse()

    def _get_volume(self):
        '''Returns volume level.'''
        sink = self.pulsectl.sink_list()[self.SINK_ID]
        vol = self.pulsectl.volume_get_all_chans(sink)
        return vol

    def _get_mute(self):
        '''Returns mute state.'''
        sink = self.pulsectl.sink_list()[self.SINK_ID]
        return sink.mute

    def _set_mute(self, state):
        '''Set mute state.'''
        sink = self.pulsectl.sink_list()[self.SINK_ID]
        self.pulsectl.mute(sink, state)

    def _set_exact(self, state):
        '''Set exact volume level.'''
        vol = None

        try:
            vol = float(state)
        except ValueError:
            return

        if vol >= 0 and vol <= 1:
            sink = self.pulsectl.sink_list()[self.SINK_ID]
            self.pulsectl.volume_set_all_chans(sink, vol)

    def _turn_up(self):
        '''Turn up volume level.'''
        vol = self._get_volume() + 0.05
        if vol >= 0 and vol <= 1:
            sink = self.pulsectl.sink_list()[self.SINK_ID]
            self.pulsectl.volume_set_all_chans(sink, vol)

    def _turn_down(self):
        '''Turn down volume level.'''
        vol = self._get_volume() - 0.05
        if vol >= 0 and vol <= 1:
            sink = self.pulsectl.sink_list()[self.SINK_ID]
            self.pulsectl.volume_set_all_chans(sink, vol)

    def _pub(self, topic, msg):
        '''Publish message to topic.'''
        self.mqtt.publish(topic, payload=msg, retain=True)

    def loop_start(self):
        '''Publish updates.'''
        while True:
            vol = round(100*self._get_volume())  # in % please
            self._pub(self.TOPIC_PUB_VOL, vol)

            if self._get_mute():
                self._pub(self.TOPIC_PUB_SOUND, 'OFF')
            else:
                self._pub(self.TOPIC_PUB_SOUND, 'ON')

            time.sleep(self.TIMEOUT)

    def on_connect(self, client, userdata, flags, rc):
        '''Called when the broker responds to our connection request.'''
        client.subscribe(self.TOPIC_SUB)

    def on_message(self, client, userdata, msg):
        '''Called when a message has been received.'''
        if msg.topic == self.TOPIC_SUB:
            payload = bytes.decode(msg.payload)
            if payload == 'OFF':
                self._set_mute(1)
            elif payload == 'ON':
                self._set_mute(0)
            elif payload == 'TURN_UP':
                self._turn_up()
            elif payload == 'TURN_DOWN':
                self._turn_down()
            else:
                self._set_exact(payload)
