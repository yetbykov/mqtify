import time
import ctypes
import struct
import os

from yapsy.IPlugin import IPlugin
from yapsy.PluginManager import PluginManagerSingleton

ctypes.cdll.LoadLibrary('libXext.so')
libXext = ctypes.CDLL('libXext.so')


class dpms(IPlugin):
    '''DPMS plugin.

    Control DPMS from remote.
    '''
    STATE_NONE = -2
    STATE_FAIL = -1
    STATE_ON = 0
    STATE_STANDBY = 1
    STATE_SUSPEND = 2
    STATE_OFF = 3
    CONF_SECTION = 'DPMS'
    DISPLAY = ':0'
    TOPIC_PUB = '/mqtify/dpms/state'
    TOPIC_SUB = '/mqtify/dpms/set'
    TIMEOUT = 5

    def __init__(self):
        super().__init__()
        self._setup_mqtify_attr()
        self._setup_conf()

    def _setup_mqtify_attr(self):
        '''Get attributes from Mqtify using manager singleton.'''
        manager = PluginManagerSingleton.get()
        self.mqtt = manager.mqtify.mqtt
        self.conf = manager.mqtify.conf

    def _setup_conf(self):
        '''Read configuration.'''
        if self.CONF_SECTION in self.conf:
            conf = self.conf[self.CONF_SECTION]
            if 'display' in conf:
                self.DISPLAY = str(conf['display'])
            if 'topic_pub' in conf:
                self.TOPIC_PUB = str(conf['topic_pub'])
            if 'topic_sub' in conf:
                self.TOPIC_SUB = str(conf['topic_sub'])
            if 'timeout' in conf:
                self.TIMEOUT = int(conf['timeout'])

    def _get_dpms_state(self, display):
        '''Returns DPMS state.'''
        display = display.encode('ascii')
        state = self.STATE_FAIL
        display_name = ctypes.c_char_p()
        display_name.value = display
        libXext.XOpenDisplay.restype = ctypes.c_void_p
        display = ctypes.c_void_p(libXext.XOpenDisplay(display_name))
        dummy1_i_p = ctypes.create_string_buffer(8)
        dummy2_i_p = ctypes.create_string_buffer(8)
        if display.value:
            if libXext.DPMSQueryExtension(display, dummy1_i_p, dummy2_i_p)\
               and libXext.DPMSCapable(display):
                onoff_p = ctypes.create_string_buffer(1)
                state_p = ctypes.create_string_buffer(2)
                if libXext.DPMSInfo(display, state_p, onoff_p):
                    onoff = struct.unpack('B', onoff_p.raw)[0]
                    if onoff:
                        state = struct.unpack('H', state_p.raw)[0]
            libXext.XCloseDisplay(display)
        return state

    def _pub(self, msg):
        '''Publish message to TOPIC_PUB.'''
        self.mqtt.publish(self.TOPIC_PUB, payload=msg, retain=True)

    def loop_start(self):
        '''Publish updates.'''
        while True:
            state = self._get_dpms_state(self.DISPLAY)

            if state == self.STATE_NONE\
               or state == self.STATE_FAIL\
               or state == self.STATE_STANDBY\
               or state == self.STATE_SUSPEND\
               or state == self.STATE_OFF:
                self._pub('OFF')
            else:
                self._pub('ON')

            time.sleep(self.TIMEOUT)

    def on_connect(self, client, userdata, flags, rc):
        '''Called when the broker responds to our connection request.'''
        client.subscribe(self.TOPIC_SUB)

    def on_message(self, client, userdata, msg):
        '''Called when a message has been received.'''
        if msg.topic == self.TOPIC_SUB:
            payload = bytes.decode(msg.payload)
            if payload == 'OFF':
                os.system('xset dpms force off')
            elif payload == 'ON':
                os.system('xset dpms force on')
