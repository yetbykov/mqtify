import time
import psutil

from yapsy.IPlugin import IPlugin
from yapsy.PluginManager import PluginManagerSingleton


class crd(IPlugin):
    '''CPU, RAM and disk monitoring.'''
    CONF_SECTION = 'CRD'
    TOPIC_PUB_CPU_LOAD = '/mqtify/crd/cpu/load'
    TOPIC_PUB_CPU_TEMP = '/mqtify/crd/cpu/temp'
    TOPIC_PUB_MEM_USED = '/mqtify/crd/mem/used'
    TOPIC_PUB_DISK_FREE = '/mqtify/crd/disk/free'
    TIMEOUT = 60
    MODULES = ['cpu_load', 'cpu_temp', 'mem_used', 'disk_free']
    SENSOR_NAME = 'coretemp'
    SENSOR_ID = 0
    DISK_PATH = '/'

    def __init__(self):
        super().__init__()
        self._setup_mqtify_attr()
        self._setup_conf()

    def _setup_mqtify_attr(self):
        '''Get attributes from Mqtify using manager singleton.'''
        manager = PluginManagerSingleton.get()
        self.mqtt = manager.mqtify.mqtt
        self.conf = manager.mqtify.conf

    def _setup_conf(self):
        '''Read configuration.'''
        if self.CONF_SECTION in self.conf:
            conf = self.conf[self.CONF_SECTION]
            if 'display' in conf:
                self.DISPLAY = str(conf['display'])
            if 'topic_pub_cpu_load' in conf:
                self.TOPIC_PUB_CPU_LOAD = str(conf['topic_pub_cpu_load'])
            if 'topic_pub_cpu_temp' in conf:
                self.TOPIC_PUB_CPU_TEMP = str(conf['topic_pub_cpu_temp'])
            if 'topic_pub_mem_used' in conf:
                self.TOPIC_PUB_MEM_USED = str(conf['topic_pub_mem_used'])
            if 'topic_pub_disk_free' in conf:
                self.TOPIC_PUB_DISK_FREE = str(conf['topic_pub_disk_free'])
            if 'timeout' in conf:
                self.TIMEOUT = int(conf['timeout'])
            if 'modules' in conf:
                self.MODULES = str(conf['modules']).split(';;')
            if 'sensors_name' in conf:
                self.SENSOR_NAME = str(conf['sensor_name'])
            if 'sensor_id' in conf:
                self.SENSOR_ID = int(conf['sensor_id'])
            if 'disk_path' in conf:
                self.DISK_PATH = str(conf['disk_path'])

    def _pub(self, topic, msg):
        '''Publish message to topic.'''
        self.mqtt.publish(topic, payload=msg, retain=True)

    def loop_start(self):
        '''Publish updates.'''
        while True:
            if 'cpu_load' in self.MODULES:
                load = round(psutil.cpu_percent())
                self._pub(self.TOPIC_PUB_CPU_LOAD, load)

            if 'cpu_temp' in self.MODULES:
                sensor = psutil.sensors_temperatures()[self.SENSOR_NAME]
                temp = round(sensor[self.SENSOR_ID].current)
                self._pub(self.TOPIC_PUB_CPU_TEMP, temp)

            if 'mem_used' in self.MODULES:
                usage = round(psutil.virtual_memory().percent)
                self._pub(self.TOPIC_PUB_MEM_USED, usage)

            if 'disk_free' in self.MODULES:
                free = round(psutil.disk_usage(self.DISK_PATH).free/1073741824)
                self._pub(self.TOPIC_PUB_DISK_FREE, free)

            time.sleep(self.TIMEOUT)
