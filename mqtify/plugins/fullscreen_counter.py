import Xlib.display
import time

from yapsy.IPlugin import IPlugin
from yapsy.PluginManager import PluginManagerSingleton


class fullscreen_counter(IPlugin):
    '''Fullscreen counter plugin.

    Counts fullscreen apps on host.
    '''
    CONF_SECTION = 'Fullscreen Counter'
    TOPIC_PUB = '/mqtify/fs/state'
    TIMEOUT = 30

    def __init__(self):
        super().__init__()
        self._setup_mqtify_attr()
        self._setup_conf()
        self._setup_screen()

    def _setup_mqtify_attr(self):
        '''Get attributes from Mqtify using manager singleton.'''
        manager = PluginManagerSingleton.get()
        self.mqtt = manager.mqtify.mqtt
        self.conf = manager.mqtify.conf

    def _setup_conf(self):
        '''Read configuration.'''
        if self.CONF_SECTION in self.conf:
            conf = self.conf[self.CONF_SECTION]
            if 'topic_pub' in conf:
                self.TOPIC_PUB = str(conf['topic_pub'])
            if 'timeout' in conf:
                self.TIMEOUT = int(conf['timeout'])

    def _setup_screen(self):
        '''Get screen from Xlib.'''
        self.screen = Xlib.display.Display().screen()
        self.root_win = self.screen.root

    def _get_num_of_fs_apps(self, screen, root_win):
        '''Returns number of fullscreen apps.

        Adapted from https://stackoverflow.com/a/1360522
        '''
        num_of_fs = 0
        for window in root_win.query_tree()._data['children']:
            width = window.get_geometry()._data["width"]
            height = window.get_geometry()._data["height"]

            if width == screen.width_in_pixels\
               and height == screen.height_in_pixels:
                num_of_fs += 1

        return num_of_fs

    def _pub(self, msg):
        '''Publish message to TOPIC_PUB.'''
        self.mqtt.publish(self.TOPIC_PUB, payload=msg, retain=True)

    def loop_start(self):
        '''Publish updates.'''
        while True:
            state = self._get_num_of_fs_apps(self.screen, self.root_win)
            self._pub(state)
            time.sleep(self.TIMEOUT)
