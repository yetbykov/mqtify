import time

from yapsy.IPlugin import IPlugin
from yapsy.PluginManager import PluginManagerSingleton
from voluptuous import Schema, Required, All, Length, Range, Coerce


class availability(IPlugin):
    '''Availability plugin.

    Publishes availability updates.
    '''
    CONF_SECTION = 'Availability'
    TOPIC_PUB = '/mqtify/availability'
    TOPIC_SUB = '/mqtify/#'
    TIMEOUT = 30
    schema = Schema({
        Required('topic_pub', default=TOPIC_PUB): All(str, Length(min=1)),
        Required('topic_sub', default=TOPIC_SUB): All(str, Length(min=1)),
        Required('timeout', default=TIMEOUT): All(Coerce(int), Range(min=1)),
    })

    def __init__(self):
        super().__init__()
        self._setup_mqtify_attr()
        self._setup_conf()

    def _setup_mqtify_attr(self):
        '''Get attributes from Mqtify using manager singleton.'''
        manager = PluginManagerSingleton.get()
        self.mqtt = manager.mqtify.mqtt
        self.conf = manager.mqtify.conf

    def _setup_conf(self):
        '''Read configuration.'''
        validate = {}
        if self.CONF_SECTION in self.conf:
            conf = self.conf[self.CONF_SECTION]
            if 'topic_pub' in conf:
                validate['topic_pub'] = conf['topic_pub']
            if 'topic_sub' in conf:
                validate['topic_sub'] = conf['topic_sub']
            if 'timeout' in conf:
                validate['timeout'] = conf['timeout']

        conf = self.schema(validate)
        self.TOPIC_PUB = conf['topic_pub']
        self.TOPIC_SUB = conf['topic_sub']
        self.TIMEOUT = conf['timeout']

    def _pub(self, msg):
        '''Publish message to TOPIC_PUB.'''
        self.mqtt.publish(self.TOPIC_PUB, payload=msg, retain=True)

    def loop_start(self):
        '''Publish updates.'''
        while True:
            self._pub('ON')
            time.sleep(self.TIMEOUT)

    def on_connect(self, client, userdata, flags, rc):
        '''Called when the broker responds to our connection request.'''
        client.subscribe(self.TOPIC_SUB)

    def on_exit(self, exc_type, exc_value, traceback):
        '''Called on exit.'''
        self._pub('OFF')
