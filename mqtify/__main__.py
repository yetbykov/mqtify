import os
import logging
import _thread

from argparse import ArgumentParser
from configparser import ConfigParser

import paho.mqtt.client as mqtt

from yapsy.VersionedPluginManager import VersionedPluginManager
from yapsy.PluginManager import PluginManagerSingleton
from xdg.BaseDirectory import save_config_path, xdg_data_dirs
from voluptuous import Schema, Required, All, Length, Range, Coerce


class Mqtify():
    '''Bridge between your Unix box and an MQTT server.'''
    APP_NAME = 'mqtify'
    ADDRESS = 'localhost'
    PORT = 1883
    KEEPALIVE = 60
    USERNAME = None
    PASSWORD = None
    CA_CERTS = None
    CERTFILE = None
    KEYFILE = None
    PLUGINS_ACTIVATE = ['Availability']
    schema = Schema({
        Required('address', default=ADDRESS): All(str, Length(min=1)),
        Required('port', default=PORT): All(Coerce(int),
                                            Range(min=0, max=65535)),
        Required('keepalive', default=KEEPALIVE): Coerce(int),
        Required('username', default=USERNAME): All(str, Length(min=1)),
        Required('password', default=PASSWORD): All(str, Length(min=1)),
        Required('ca_certs', default=CA_CERTS): All(str, Length(min=1)),
        Required('certfile', default=CERTFILE): All(str, Length(min=1)),
        Required('keyfile', default=KEYFILE): All(str, Length(min=1)),
        Required('activate', default=PLUGINS_ACTIVATE): [str],
    })

    def __init__(self):
        super().__init__()
        self._setup_conf()
        self._setup_mqtt()
        self._setup_manager()

    def __enter__(self):
        return self

    def _setup_conf(self):
        '''Setup configuration instance.

        Looks for configuration file in standart save_config_path:
        - ~/.config/app/app.conf

        Overrides defaults that are set in this class.
        '''
        self.conf = ConfigParser()
        config_path = save_config_path(self.APP_NAME)
        config_file = os.path.join(config_path, self.APP_NAME + '.conf')
        logging.debug('Reading configuration file: ' + config_file)
        self.conf.read(config_file)

        # Read configuration to temporary dict
        validate = {}
        if 'Broker' in self.conf:
            conf = self.conf['Broker']
            if 'address' in conf:
                validate['address'] = conf['address']
            if 'port' in conf:
                validate['port'] = conf['port']
            if 'keepalive' in conf:
                validate['keepalive'] = conf['keepalive']
            if 'username' in conf:
                validate['username'] = conf['username']
            if 'password' in conf:
                validate['password'] = conf['password']
            if 'ca_certs' in conf:
                validate['ca_certs'] = conf['ca_certs']
            if 'certfile' in conf:
                validate['certfile'] = conf['certfile']
            if 'keyfile' in conf:
                validate['keyfile'] = conf['keyfile']
        if 'Plugins' in self.conf:
            conf = self.conf['Plugins']
            if 'activate' in conf:
                validate['activate'] = conf['activate'].split(';;')

        # Validate temporary dict and override defaults
        conf = self.schema(validate)
        self.ADDRESS = conf['address']
        self.PORT = conf['port']
        self.KEEPALIVE = conf['keepalive']
        self.USERNAME = conf['username']
        self.PASSWORD = conf['password']
        self.CA_CERTS = conf['ca_certs']
        self.CERTFILE = conf['certfile']
        self.KEYFILE = conf['keyfile']
        self.PLUGINS_ACTIVATE = conf['activate']

    def _setup_mqtt(self):
        '''Setup MQTT instance.

        By default, connect is done without TLS and any authentication data.
        '''
        self.mqtt = mqtt.Client(self.APP_NAME)

        self.mqtt.on_connect = self._on_connect
        self.mqtt.on_message = self._on_message

        if self.CA_CERTS:
            self.mqtt.tls_set(self.CA_CERTS,
                              certfile=self.CERTFILE,
                              keyfile=self.KEYFILE)

        if self.USERNAME and self.PASSWORD:
            self.mqtt.username_pw_set(self.USERNAME, password=self.PASSWORD)
        elif self.USERNAME:
            self.mqtt.username_pw_set(self.USERNAME)

        self.mqtt.connect(self.ADDRESS, self.PORT, self.KEEPALIVE)

    def _setup_manager(self):
        '''Setup manager singleton.

        This routine looks for plugins in standart xdg_data_dirs places:
        - ~/.local/share/app/plugins
        - /usr/local/share/app/plugins
        - /usr/share/app/plugins
        It will also look for plugins in a directory where this module is
        located.

        It will only load and activate plugins that are in PLUGINS_ACTIVATE
        list.

        After setup, any plugin with access to manager singleton can call
        this class using manager.mqtify.

        Note: PluginManagerSingleton.setBehaviour() should be called before
        any call to PluginManagerSingleton.get(), otherwise it will ignore
        anything we say here.
        '''
        PluginManagerSingleton.setBehaviour([
            VersionedPluginManager,  # Load only the newest versions on plugins
        ])

        app_dir = os.path.abspath(os.path.dirname(__file__))
        plugin_dir = os.path.join(app_dir, 'plugins')
        plugin_places = [plugin_dir, ]
        for path in xdg_data_dirs:
            plugin_places.append(os.path.join(path, self.APP_NAME, 'plugins'))

        manager = PluginManagerSingleton.get()
        manager.mqtify = self  # Plugins are allowed to access us

        manager.setPluginInfoExtension("plugin")
        manager.setPluginPlaces(plugin_places)

        manager.locatePlugins()

        for candidate_tuple in manager.getPluginCandidates():
            plugin_info = candidate_tuple[2]
            if plugin_info.name not in self.PLUGINS_ACTIVATE:
                manager.removePluginCandidate(candidate_tuple)
                logging.debug('Removing candidate: ' + plugin_info.name)

        manager.loadPlugins()

        for plugin in self.PLUGINS_ACTIVATE:
            manager.activatePluginByName(plugin)

    def loop_start(self):
        '''Start loop method for both plugins and MQTT.

        Note that every loop_start() method for plugin is using it's own
        thread; thus, blocking routine before calling MQTT loop_forever()
        method is avoided.
        '''
        manager = PluginManagerSingleton.get()
        for plugin in manager.getAllPlugins():
            if plugin.is_activated\
               and hasattr(plugin.plugin_object, 'loop_start'):
                _thread.start_new_thread(plugin.plugin_object.loop_start, ())
        self.mqtt.loop_forever()

    def _on_connect(self, client, userdata, flags, rc):
        '''Called when the broker responds to our connection request.'''
        manager = PluginManagerSingleton.get()
        for plugin in manager.getAllPlugins():
            if plugin.is_activated\
               and hasattr(plugin.plugin_object, 'on_connect'):
                plugin.plugin_object.on_connect(client, userdata, flags, rc)

    def _on_message(self, client, userdata, msg):
        '''Called when a message has been received.'''
        logging.debug('got msg: '+msg.topic+' '+bytes.decode(msg.payload))
        manager = PluginManagerSingleton.get()
        for plugin in manager.getAllPlugins():
            if plugin.is_activated\
               and hasattr(plugin.plugin_object, 'on_message'):
                plugin.plugin_object.on_message(client, userdata, msg)

    def __exit__(self, exc_type, exc_value, traceback):
        '''Called on exit.'''
        manager = PluginManagerSingleton.get()
        for plugin in manager.getAllPlugins():
            if plugin.is_activated \
               and hasattr(plugin.plugin_object, 'on_exit'):
                plugin.plugin_object.on_exit(exc_type, exc_value, traceback)
        self.mqtt.loop_stop()


def main():
    parser = ArgumentParser()
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    with Mqtify() as mqtify:
        try:
            mqtify.loop_start()
        except KeyboardInterrupt:
            print('Bye')


if __name__ == "__main__":
    main()
