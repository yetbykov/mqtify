===========
DPMS plugin
===========
Control DPMS from remote

***************
Payload
***************
You can send these payloads to ``topic_sub``:

- ``OFF`` - Force off DPMS
- ``ON`` - Force on DPMS

You can receive these payloads from ``topic_pub``:

- ``OFF`` - DPMS is in state NONE/FAIL/STANDBY/SUSPEND/OFF
- ``ON`` - DPMS is ON

***************
Configuration
***************

.. code-block:: ini

    [DPMS]
    # Name of the display
    display = :0
    
    # Topic to publush
    topic_pub = /mqtify/dpms/state

    # Topic to subscribe
    topic_sub = /mqtify/dpms/set

    # Time to wait before next publish
    timeout = 5
