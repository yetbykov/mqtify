================
PulseAudio plugin
================
Control PulseAudio from remote

***************
Payload
***************
You can send these payloads to ``topic_sub``:

- ``OFF`` - Mutes sink
- ``ON`` - Unmutes sink
- ``TURN_UP`` - Turn up volume
- ``TURN_DOWN`` - Turn down volume
- **Float** from 0 to 1 - Set exact volume level

You can receive these payloads from ``topic_pub_sound``:

- ``OFF`` - Sink is muted
- ``ON`` - Sink is unmuted

You can receive **volume level** in percents from ``topic_pub_vol``.
  
***************
Dependencies
***************
This plugin depends on pulsectl_. You can install it using pip:

- ``pip install 'mqtify[pulseaudio]'``

.. _pulsectl: https://pypi.python.org/pypi/pulsectl

***************
Configuration
***************

.. code-block:: ini

    [PulseAudio]
    # Topic to publish volume level updates
    topic_pub_vol = /mqtify/pulseaudio/vol
    
    # Topic to publish mute updates
    topic_pub_sound = /mqtify/pulseaudio/sound

    # Topic to subscribe
    topic_sub = /mqtify/pulseaudio/set

    # Time to wait before next publish
    timeout = 15

    # Sink ID. You can get it using 'pamixer --list-sinks'.
    sink_id = 0
