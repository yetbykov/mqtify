=========================
Fullscreen Counter plugin
=========================
Counts fullscreen apps on host.

Example usecase: dim room lights when application raised to fullscreen.

***************
Payload
***************
You can receive **number of fullscreen apps** from ``topic_pub``.

***************
Dependencies
***************
This plugin depends on python-xlib_. You can install it using pip:

- ``pip install 'mqtify[fullscreen_counter]'``

.. _python-xlib: https://github.com/python-xlib/python-xlib

***************
Configuration
***************

.. code-block:: ini

    [Fullscreen Counter]
    # Topic to publush
    topic_pub = /mqtify/fs/state

    # Time to wait before next publish
    timeout = 30
