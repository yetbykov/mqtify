=============
Shell plugin
=============
Execute shell commands on host.

Example usecases are:

- Push notifications to PC
- Start/stop/restart services
- Control brightness of screen depending on scene
- Suspend devices when you leave home
- ...

Note that you should properly setup your MQTT broker to deny **anyone** publishing
at this topic, as it will execute **anything** it will gets as a payload!

***************
Payload
***************
You can **execute shell** by sending command as a payload to ``topic_sub``.

- ``mosquitto_pub -t "/mqtify/sh/run" -m 'notify-send "Hello world"'``
- ``mosquitto_pub -t "/mqtify/sh/run" -m 'systemctl --user restart syncthing.service'``
- ``mosquitto_pub -t "/mqtify/sh/run" -m 'xrandr --output HDMI-1 --off'``
 
***************
Configuration
***************

.. code-block:: ini

    [Shell]
    # Topic to subscribe (should be accessed ONLY by you!)
    topic_sub = /mqtify/sh/run

    # String of commands allowed to execute. Should be separated by ;;
    whitelist = None
