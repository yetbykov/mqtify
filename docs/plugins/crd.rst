================
CRD plugin
================
CPU, RAM and disk monitoring.

***************
Payload
***************
What's you can receive:

- **CPU load** in percents from ``topic_pub_cpu_load``
- **CPU temperature** from ``topic_pub_cpu_temp``
- **Used memory** in percents from ``topic_pub_mem_used``
- **Free disk space** on / in GB from ``topic_pub_disk_free``

***************
Dependencies
***************
This plugin depends on psutil_. You can install it using pip:

- ``pip install 'mqtify[crd]'``

.. _psutil: https://pypi.python.org/pypi/psutil

***************
Configuration
***************

.. code-block:: ini

    [CRD]
    # Topic to publish CPU load updates
    topic_pub_cpu_load = /mqtify/crd/cpu/load

    # Topic to publish CPU temp updates
    topic_pub_cpu_temp = /mqtify/crd/cpu/temp

    # Topic to publish free memory updates
    topic_pub_mem_used = /mqtify/crd/mem/used

    # Topic to publish free space updates
    topic_pub_disk_free = /mqtify/crd/disk/free

    # Time to wait before next publish
    timeout = 60

    # Modules to load. Separated by double semicolon
    modules = cpu_load;;cpu_temp;;mem_used;;disk_free

    # Sensor. You can get it from 'sensors'
    sensor_name = coretemp

    # Sensor ID. You can get it from 'sensors'
    sensor_id = 0

    # Disk path
    disk_path = /
