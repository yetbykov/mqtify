===============
List of plugins
===============

.. toctree::
   :maxdepth: 2

   availability
   dpms
   alsaaudio
   pulseaudio
   crd
   sh
   fullscreen_counter
