.. Mqtify documentation master file, created by
   sphinx-quickstart on Thu Dec 21 10:54:46 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Mqtify
======

Welcome to the documentation for mqtify, the bridge between your Unix box and
an MQTT server.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   guides/configuration
   plugins/index
   guides/developing_a_plugin

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
