===================
Developing a plugin
===================
Plugins written entirely in Python using Yapsy plugin management.

Mqtify looks for plugins in this places:

- ``~/.local/share/mqtify/plugins``
- ``/usr/local/share/mqtify/plugins``
- ``/usr/share/mqtify/plugins``

***************
Plugin info
***************
Plugin info stored in a plain text file with ``.plugin`` extension. Here is an
example of how this file should look like:

.. code-block:: ini

    [Core]
    Name = Availability
    Module = availability

    [Documentation]
    Author = Bykov Vladislav
    Version = 1.0
    Website = https://github.com/envolyse/mqtify
    Description = Publishes availability updates.

Note that ``[Documentation]`` part is optional and you can always skip that if
you like.

***************
Module
***************
This is an example with all APIs Mqtify gives you.

.. code-block:: python

    from yapsy.IPlugin import IPlugin
    from yapsy.PluginManager import PluginManagerSingleton


    class myplug(IPlugin):
        CONF_SECTION = 'Myplug'
        TOPIC_PUB = '/mytopic'

        def __init__(self):
            super().__init__()
            self.__setup_mqtify_attr()
            self.__setup_conf()

        def __setup_mqtify_attr(self):
            manager = PluginManagerSingleton.get()
            self.mqtt = manager.mqtify.mqtt  # Get MQTT instance from Mqtify
            self.conf = manager.mqtify.conf  # Get conf instance from Mqtify

        def __setup_conf(self):
            # Check whether user configuration have plugin's section
            if self.CONF_SECTION in self.conf:
                # If so, override defaults
                conf = self.conf[self.CONF_SECTION]
                if 'topic_pub' in conf:
                    self.TOPIC_PUB = str(conf['topic_pub'])

        def loop_start(self):
            # Publishes 'ON' at TOPIC_PUB every 15 seconds
            while True:
                self.mqtt.publish(self.TOPIC_PUB, payload='ON', retain=True)
                time.sleep(15)

        def on_connect(self, client, userdata, flags, rc):
            # You can subscribe to your topic here
            # client.subscribe(self.TOPIC_SUB)
            pass

        def on_message(self, client, userdata, msg):
            # You can react to received messages here
            # if msg.topic == self.TOPIC_SUB:
            #    if bytes.decode(msg.payload) == 'ON':
            #        subprocess.Popen(self.CMD)
            pass

        def on_exit(self, exc_type, exc_value, traceback):
            # Publishes 'OFF' when app is closed
            self.mqtt.publish(self.TOPIC_PUB, payload="OFF", retain=True)

Yes, it's that simple! By the way, you can safely import any Python library
you need for your plugin.

For more information on this attributes see API documentation here:
:ref:`modindex` (or here :ref:`genindex`).
