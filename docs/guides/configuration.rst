=============
Configuration
=============
All configration is done by editing ``~/.config/mqtify/mqtify.conf``.

***************
Plugins
***************

.. code-block:: ini

    [Plugins]
    # List of plugins to activate. Separated by ;;
    # Default: Availability
    activate = Availability;;DPMS;;Shell

To configure plugins, please add their section somewhere in the file.
For additional configuration see :doc:`/plugins/index`.

***************
Broker
***************
.. code-block:: ini

    [Broker]
    # Broker address
    # Default: localhost
    address = 192.168.2.3

    # Broker port
    # Default: 1883
    port = 1234

    # Broker keepalive
    # Default: 60
    keepalive = 120

    # Username to authenticate with
    # Default: <empty>
    username = myusername

    # Password to authenticate with
    # Default: <empty>
    password = mypassword

    # Path to ca_certs file
    # Default: <empty>
    ca_certs =

    # Default: 
    # Path to certfile
    # Default: <empty>
    certfile =

    # Path to keyfile
    # Default: <empty>
    keyfile =
