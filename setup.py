#!/usr/bin/env python

from setuptools import setup, find_packages

try:
    import pypandoc
    long_description = pypandoc.convert('README.md', 'rst')
except(IOError, ImportError):
    long_description = open('README.md').read()

setup(name='mqtify',
      version='0.3.3',
      description='Bridge between your Unix box and an MQTT server',
      long_description=long_description,
      url='https://github.com/envolyse/mqtify',
      author='Bykov Vladislav',
      author_email='envolyse@gmail.com',
      license='MIT',
      classifiers=[
          'Development Status :: 4 - Beta',
          'Intended Audience :: End Users/Desktop',
          'Topic :: Home Automation',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3',
      ],
      keywords='mqtt python bridge modular iot smarthome unix',
      platforms='any',
      packages=find_packages(),
      install_requires=[
          'yapsy',
          'paho-mqtt',
          'pyxdg',
          'voluptuous'
      ],
      python_requires='>=3',
      extras_require={
          'alsaaudio':  ['pyalsaaudio'],
          'pulseaudio': ['pulsectl'],
          'crd': ['psutil'],
          'fullscreen_counter': ['python-xlib'],
      },
      package_data={
          'mqtify.plugins': ['*.plugin'],
      },
      entry_points={
          'console_scripts': [
              'mqtify = mqtify.__main__:main'
          ]
      },
      )
